# LAB Hackathon example



![picture](./pictures/lab-hackathon.png)


## Links to project APIs
[Frontend](http://localhost:8080)

[API](http://localhost:8080/api)

[API Docs](http://localhost:8080/docs/api)

[V3 OpenAPI](http://localhost:8080/docs/v3/api)



![picture](./pictures/UI.png)

## Setup
[GitLab link](https://gitlab.com/matiaspajuvesa/lab-hackathon-2023-example)

### Required development software
Developed on Ubuntu 20.04.5 LTS
* docker-compose version 1.28.3, build 14736152
* Docker version 19.03.14, build 5eb3275d40
* Node.js v14.16.1
* npm 6.14.12

### Backend
Create [environment file](./backend/.env)
```
APP_PORT=3000
LOG_LEVEL=debug
JWT_SECRET=SUPERSECRET
DATA_PATH=/server/data
```

Create `JWT_SECRET` from linux terminal
```sh
tr -dc A-Za-z0-9 </dev/urandom | head -c 13 ; echo ''
```

### Running
```sh
docker-compose build
docker-compose up
```

Create admin user
```sh
curl -X POST http://localhost:8080/api/auth/register
   -H 'Content-Type: application/json'
   -d '{"username":"admin","password":"password"}'
```
