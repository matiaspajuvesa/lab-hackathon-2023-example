import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor() { }

  @Input() activeTab: 'firmware' | 'filepack' = 'firmware';
  @Output() tabClickedEmit = new EventEmitter();

  ngOnInit(): void {
  }

  tabClicked(tab: 'firmware' | 'filepack') {
    this.tabClickedEmit.next(tab);
  }

}
