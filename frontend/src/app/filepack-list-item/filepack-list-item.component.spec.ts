import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilepackListItemComponent } from './filepack-list-item.component';

describe('FilepackListItemComponent', () => {
  let component: FilepackListItemComponent;
  let fixture: ComponentFixture<FilepackListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilepackListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepackListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
