import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmwareListFilterComponent } from './firmware-list-filter.component';

describe('FirmwareListFilterComponent', () => {
  let component: FirmwareListFilterComponent;
  let fixture: ComponentFixture<FirmwareListFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmwareListFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmwareListFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
