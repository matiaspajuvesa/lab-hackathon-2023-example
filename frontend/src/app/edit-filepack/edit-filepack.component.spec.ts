import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFilepackComponent } from './edit-filepack.component';

describe('EditFilepackComponent', () => {
  let component: EditFilepackComponent;
  let fixture: ComponentFixture<EditFilepackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFilepackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFilepackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
