import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilepackService } from '../filepack.service';

@Component({
  selector: 'app-edit-filepack',
  templateUrl: './edit-filepack.component.html',
  styleUrls: ['./edit-filepack.component.scss']
})
export class EditFilepackComponent implements OnInit {

  @Input() id!: string;
  @Input() name: string | undefined;
  @Input() version: string | undefined;
  @Input() deviceId: string | undefined;
  @Input() description: string | undefined;

  @Output() firmwareEditedEmit = new EventEmitter();

  constructor(
    private filepackService: FilepackService
  ) { }

  ngOnInit(): void {
  }

  async update() {
    const response = await this.filepackService.update({
      id: this.id,
      name: this.name,
      version: this.version,
      deviceId: this.deviceId,
      description: this.description
    });

    this.firmwareEditedEmit.emit();
  }
}
