import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = 'lab-hackathon2023-frontend-example';

  activeTab: 'firmware' | 'filepack' = 'firmware';

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  tabChanged(tab: 'firmware' | 'filepack') {
    this.activeTab = tab;
    this.cdr.markForCheck();
  }
}
