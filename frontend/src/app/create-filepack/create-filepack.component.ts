import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FilepackService } from '../filepack.service';

@Component({
  selector: 'app-create-filepack',
  templateUrl: './create-filepack.component.html',
  styleUrls: ['./create-filepack.component.scss']
})
export class CreateFilepackComponent implements OnInit {
  name: string | undefined;
  version: string | undefined;
  deviceId: string | undefined;
  description: string | undefined;

  @Output() createdEmit = new EventEmitter();

  constructor(
    private filepackService: FilepackService
  ) { }

  ngOnInit(): void {
  }

  async create() {
    const response = await this.filepackService.create({
      name: this.name,
      version: this.version,
      deviceId: this.deviceId,
      description: this.description
    });

    this.createdEmit.emit();
  }
}
