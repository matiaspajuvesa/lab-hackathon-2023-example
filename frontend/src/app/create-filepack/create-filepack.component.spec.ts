import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFilepackComponent } from './create-filepack.component';

describe('CreateFilepackComponent', () => {
  let component: CreateFilepackComponent;
  let fixture: ComponentFixture<CreateFilepackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFilepackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFilepackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
