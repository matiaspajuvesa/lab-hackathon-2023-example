import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filepack-list-filter',
  templateUrl: './filepack-list-filter.component.html',
  styleUrls: ['./filepack-list-filter.component.scss']
})
export class FilepackListFilterComponent implements OnInit {

  @Input() nameFilter: string | undefined = undefined;
  @Input() typeFilter: string | undefined = undefined;
  @Input() versionFilter: string | undefined = undefined;

  @Output() nameFilterEmit = new EventEmitter();
  @Output() typeFilterEmit = new EventEmitter();
  @Output() versionFilterEmit = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  nameChange(value: string) {
    this.nameFilterEmit.emit(value && value.length > 0 ? value : undefined);
  }
  typeChange(value: string) {
    this.typeFilterEmit.emit(value && value.length > 0 ? value : undefined);
  }
  versionChange(value: string) {
    this.versionFilterEmit.emit(value && value.length > 0 ? value : undefined);
  }

}
