import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilepackListFilterComponent } from './filepack-list-filter.component';

describe('FilepackListFilterComponent', () => {
  let component: FilepackListFilterComponent;
  let fixture: ComponentFixture<FilepackListFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilepackListFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepackListFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
