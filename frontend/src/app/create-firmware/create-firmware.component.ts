import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FirmwareService } from '../firmware.service';

@Component({
  selector: 'app-create-firmware',
  templateUrl: './create-firmware.component.html',
  styleUrls: ['./create-firmware.component.scss']
})
export class CreateFirmwareComponent implements OnInit {


  name: string | undefined;
  version: string | undefined;
  deviceId: string | undefined;
  description: string | undefined;

  @Output() createdEmit = new EventEmitter();

  constructor(
    private firmwareService: FirmwareService
  ) { }

  ngOnInit(): void {
  }

  async create() {
    const response = await this.firmwareService.create({
      name: this.name,
      version: this.version,
      deviceId: this.deviceId,
      description: this.description
    });

    this.createdEmit.emit();
  }
}
