import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface AppConfig {
  apiUrl: string;
}

@Injectable()
export class AppConfigService {

  appConfig!: AppConfig;

  constructor(
    private injector: Injector
  ) { }

  async loadConfig(): Promise<any> {
    const http = this.injector.get(HttpClient);
    const data = await http.get('/assets/config/app-config.json').toPromise();
    return this.appConfig = data as AppConfig;
  }

  get config(): AppConfig {
    return this.appConfig;
  }
}
