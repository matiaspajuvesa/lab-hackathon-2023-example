import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './app-config.service';

import { FileSaverService } from 'ngx-filesaver';
import { AuthService } from './auth.service';


export interface Firmware {
  name: string | undefined;
  version: string | undefined;
  deviceId: string | undefined;
  description: string | undefined;
};

@Injectable({
  providedIn: 'root'
})
export class FirmwareService {

  private apiUrl: string;
  constructor(
    private env: AppConfigService,
    private http: HttpClient,
    private fileSaverService: FileSaverService,
    private authService: AuthService
  ) {
    this.apiUrl = this.env.config.apiUrl;
  }

  list(): Promise<any[]> {
    return this.http.get(`${this.apiUrl}/firmware`).toPromise() as Promise<any[]>;
  }

  create(firmware: Firmware): Promise<any[]> {
    return this.http.post(`${this.apiUrl}/firmware`, firmware, {
      headers: {
        Authorization: `Bearer ${this.authService.Token()}`
      }
    }).toPromise() as Promise<any[]>;
  }
  update(firmware: Firmware & { id: string }): Promise<any[]> {
    return this.http.patch(`${this.apiUrl}/firmware/${firmware.id}`, firmware, {
      headers: {
        Authorization: `Bearer ${this.authService.Token()}`
      }
    }).toPromise() as Promise<any[]>;
  }
  delete(id: string): Promise<any> {
    return this.http.delete(`${this.apiUrl}/firmware/${id}`, {
      headers: {
        Authorization: `Bearer ${this.authService.Token()}`
      }
    }).toPromise() as Promise<any[]>;
  }

  download(id: string): Promise<any> {
    return this.http.get(`${this.apiUrl}/firmware/${id}/download`, { responseType: 'blob' }).toPromise().then(blob => {
      this.fileSaverService.save(blob)
    });
  }

  upload(id: string, opts: { data: File, name: string }): Promise<any> {
    return this.http.post(`${this.apiUrl}/firmware/${id}/upload?name=${opts.name}`, opts.data, {
      headers: {
        Authorization: `Bearer ${this.authService.Token()}`,
      }
    }).toPromise();
  }
}
