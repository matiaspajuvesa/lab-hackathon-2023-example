import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService, User } from '../auth.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopBarComponent implements OnInit {

  private onDestroy$ = new Subject<void>();
  isLoggedIn: boolean = false;

  user: User | null = null;

  constructor(
    private cdr: ChangeDetectorRef,
    private authService: AuthService
  ) { }

  async ngOnInit(): Promise<void> {
    this.authService.user$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(user => {
        this.isLoggedIn = user != null;
        this.user = user;
        this.cdr.markForCheck();
      });

    await this.login();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  async login() {
    const username = 'admin';
    const password = 'password';
    const loginSuccess = await this.authService.login(username, password);
  }
  async logout() {
    await this.authService.logout();
  }

}
