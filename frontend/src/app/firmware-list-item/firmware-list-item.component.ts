import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FirmwareService } from '../firmware.service';

@Component({
  selector: 'app-firmware-list-item',
  templateUrl: './firmware-list-item.component.html',
  styleUrls: ['./firmware-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FirmwareListItemComponent implements OnInit {

  @Input() id: string = '';
  @Input() name: string | undefined;
  @Input() version: string | undefined;
  @Input() description: string | undefined;
  @Input() file: any | undefined;
  @Input() deviceId: string | undefined;

  @Output() deleteEmit = new EventEmitter();
  @Output() downloadEmit = new EventEmitter();
  @Output() uploadEmit = new EventEmitter();
  @Output() editEmit = new EventEmitter();

  @Input() can: any;

  canEdit: boolean = false;
  canDelete: boolean = false;
  canDownload: boolean = true;
  canUpload: boolean = false;

  isOpen: boolean = false;

  constructor(
    private cdr: ChangeDetectorRef,
    private firmwareService: FirmwareService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.can) {
      const can = changes.can.currentValue;
      this.canDelete = can.delete;
      this.canUpload = can.upload;
      this.canEdit = can.edit;
      this.cdr.markForCheck();
    }
  }

  onOpenClick() {
    this.isOpen = !this.isOpen;
    this.cdr.markForCheck();
  }

  delete(file: any) {
    this.deleteEmit.emit(file);
  }
  download(file: any) {
    this.downloadEmit.emit(file);
  }
  upload() {
    this.uploadEmit.emit();
  }

  async fileChange(event: any) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      const formData: FormData = new FormData();
      formData.append('uploadFile', file, file.name);

      const done = await this.firmwareService.upload(this.id, { data: file, name: file.name });
      this.uploadEmit.next();
    }
  }

}
