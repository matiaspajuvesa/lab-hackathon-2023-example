import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmwareListItemComponent } from './firmware-list-item.component';

describe('FirmwareListItemComponent', () => {
  let component: FirmwareListItemComponent;
  let fixture: ComponentFixture<FirmwareListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FirmwareListItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmwareListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
