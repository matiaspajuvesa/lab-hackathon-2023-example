import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FirmwarePageComponent } from './firmware-page/firmware-page.component';
import { FilepackPageComponent } from './filepack-page/filepack-page.component';
import { HttpClientModule } from '@angular/common/http';
import { AppConfigService } from './app-config.service';
import { FirmwareListItemComponent } from './firmware-list-item/firmware-list-item.component';
import { FirmwareListFilterComponent } from './firmware-list-filter/firmware-list-filter.component';
import { ButtonComponent } from './button/button.component';
import { FileSaverModule } from 'ngx-filesaver';
import { FormsModule } from '@angular/forms';
import { CreateFirmwareComponent } from './create-firmware/create-firmware.component';
import { EditFirmwareComponent } from './edit-firmware/edit-firmware.component';
import { FilepackListItemComponent } from './filepack-list-item/filepack-list-item.component';
import { FilepackListFilterComponent } from './filepack-list-filter/filepack-list-filter.component';
import { CreateFilepackComponent } from './create-filepack/create-filepack.component';
import { EditFilepackComponent } from './edit-filepack/edit-filepack.component';

const appInitializerFn = (appConfig: AppConfigService) => {
  return () => {
    return appConfig.loadConfig();
  }
};
@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    NavBarComponent,
    FirmwarePageComponent,
    FilepackPageComponent,
    FirmwareListItemComponent,
    FirmwareListFilterComponent,
    ButtonComponent,
    CreateFirmwareComponent,
    EditFirmwareComponent,
    FilepackListItemComponent,
    FilepackListFilterComponent,
    CreateFilepackComponent,
    EditFilepackComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FileSaverModule,
    FormsModule
  ],
  providers: [
    AppConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [AppConfigService]
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
