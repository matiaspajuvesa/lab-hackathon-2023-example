import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfigService } from './app-config.service';
import { BehaviorSubject } from 'rxjs';
import { dot } from 'dot-object';


import jwtDecode from 'jwt-decode';

export interface User {
  username: string;
  roles: Role[];
}

export interface Role {
  id: string;
  name: string;
  permissions: {
    firmware?: Permission;
    filepack?: Permission;
  }
}

export interface Permission {
  read: boolean;
  write: boolean;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private apiUrl: string;

  private user: User | null = null;
  user$ = new BehaviorSubject<User | null>(null);
  isLoggedIn$ = new BehaviorSubject(false);

  private token: string | undefined;

  constructor(
    private env: AppConfigService,
    private http: HttpClient,
  ) {
    this.apiUrl = this.env.config.apiUrl;
  }

  async login(username: string, password: string): Promise<any> {
    const loginResponse = await this.http.post(`${this.apiUrl}/auth/login`, { username: username, password: password }).toPromise() as { token: string, validUntil: number };
    this.token = loginResponse.token;
    const decoded: any = jwtDecode(loginResponse.token);
    this.setUser({ username: decoded.username, roles: decoded.roles });
  }
  async register(username: string, password: string): Promise<any> {
    const login = await this.http.post(`${this.apiUrl}/auth/register`, { username: username, password: password }).toPromise() as Promise<any>;
  }

  async logout() {
    this.setUser(null);
  }

  private setUser(user: User | null) {
    this.user = user;
    this.user$.next(user);
    this.isLoggedIn$.next(user == null ? false : true);
  }

  Token() {
    return this.token;
  }

  canAccess(permission: string): boolean {
    if (this.user == null) { return false; }

    for (const role of this.user.roles) {
      const dotFormat = dot(role.permissions);
      if (dotFormat[permission] === true) {
        return true;
      }
    }
    return false;
  }
}
