import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService, User } from '../auth.service';
import { FilepackService } from '../filepack.service';
import { FirmwareService } from '../firmware.service';

@Component({
  selector: 'app-filepack-page',
  templateUrl: './filepack-page.component.html',
  styleUrls: ['./filepack-page.component.scss']
})
export class FilepackPageComponent implements OnInit {

  private onDestroy$ = new Subject<void>();
  user: User | null = null;

  // TODO: Add typings
  firmwares: any[] = [];
  filteredFirmwares: any[] = [];

  nameFilter: string | undefined = undefined;
  typeFilter: string | undefined = undefined;
  versionFilter: string | undefined = undefined;

  canCreateFirmware: boolean = false;

  listCan = {
    delete: false,
    upload: false,
    edit: false
  };

  constructor(
    private cdr: ChangeDetectorRef,
    private filepackService: FilepackService,
    private authService: AuthService
  ) { }

  async ngOnInit(): Promise<void> {
    this.resetAllFilters();
    const firmwares = await this.loadFilepack();

    this.authService.user$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(async user => {
        this.user = user;

        this.canCreateFirmware = this.authService.canAccess('firmware.write');

        this.listCan = {
          delete: this.canCreateFirmware,
          upload: this.canCreateFirmware,
          edit: this.canCreateFirmware
        }

        await this.loadFilepack();
        this.cdr.markForCheck();
      });
  }

  private setFilter(type: 'name' | 'type' | 'version', filter: string | undefined) {
    switch (type) {
      case 'name': { this.nameFilter = filter; break; }
      case 'type': { this.typeFilter = filter; break; }
      case 'version': { this.versionFilter = filter; break; }
    };
  }

  resetFilter(type: 'name' | 'type' | 'version') {
    this.setFilter(type, undefined);
  }
  resetAllFilters() {
    this.setFilter('name', undefined);
    this.setFilter('type', undefined);
    this.setFilter('version', undefined);
  }

  runFiltering() {
    this.filteredFirmwares = this.firmwares.filter(f => {
      const nameCheck = this.nameFilter ? f.name.toLowerCase().includes(this.nameFilter.toLowerCase()) : true;
      const typeCheck = this.typeFilter ? f.deviceId.toLowerCase().includes(this.typeFilter.toLowerCase()) : true;
      const versionCheck = this.versionFilter ? f.version.toLowerCase().includes(this.versionFilter.toLowerCase()) : true;
      const hasFileDownload = f.file != null || this.canCreateFirmware;
      return nameCheck && typeCheck && versionCheck && hasFileDownload;
    });
  }

  filterChange(type: 'name' | 'type' | 'version', filter: string | undefined = undefined) {
    this.setFilter(type, filter);
    this.runFiltering();

    this.cdr.markForCheck();
  }

  async loadFilepack() {
    const firmwares = await this.filepackService.list();
    this.firmwares = firmwares;

    this.runFiltering();

    this.cdr.markForCheck();
  }
  async onDownload(id: string) {
    await this.filepackService.download(id);
  }
  async onDelete(id: string) {
    await this.filepackService.delete(id);
    await this.loadFilepack();
  }
  async onUpload() {
    await this.loadFilepack();
  }

  async onCreate() {
    await this.loadFilepack();
  }
  async onEdit() {
    await this.loadFilepack();
  }

}
