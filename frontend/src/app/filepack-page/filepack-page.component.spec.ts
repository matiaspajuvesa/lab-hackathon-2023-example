import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilepackPageComponent } from './filepack-page.component';

describe('FilepackPageComponent', () => {
  let component: FilepackPageComponent;
  let fixture: ComponentFixture<FilepackPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilepackPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilepackPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
