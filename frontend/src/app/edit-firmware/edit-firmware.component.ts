import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FirmwareService } from '../firmware.service';

@Component({
  selector: 'app-edit-firmware',
  templateUrl: './edit-firmware.component.html',
  styleUrls: ['./edit-firmware.component.scss']
})
export class EditFirmwareComponent implements OnInit {

  @Input() id!: string;
  @Input() name: string | undefined;
  @Input() version: string | undefined;
  @Input() deviceId: string | undefined;
  @Input() description: string | undefined;

  @Output() firmwareEditedEmit = new EventEmitter();

  constructor(
    private firmwareService: FirmwareService
  ) { }

  ngOnInit(): void {
  }

  async update() {
    const response = await this.firmwareService.update({
      id: this.id,
      name: this.name,
      version: this.version,
      deviceId: this.deviceId,
      description: this.description
    });

    this.firmwareEditedEmit.emit();
  }
}
