import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFirmwareComponent } from './edit-firmware.component';

describe('EditFirmwareComponent', () => {
  let component: EditFirmwareComponent;
  let fixture: ComponentFixture<EditFirmwareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFirmwareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFirmwareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
