
import 'mocha';
import { expect } from 'chai';
import { permissionGuardCheck } from '../../src/middlewares/permission';
import { Role } from '../../src/models/user';

const ADMIN_ROLE: Role = {
    id: '4ba42d62-9462-449f-a90e-11e993ff48bf',
    name: 'admin-role',
    permissions: {
        firmware: {
            read: true,
            write: true
        },
        filepack: {
            read: false,
            write: false
        }
    }
}
const READONLY_ROLE: Role = {
    id: 'd042a9ca-a412-4c88-a51c-f110171031af',
    name: 'readonly-role',
    permissions: {
        firmware: {
            read: true,
            write: false
        },
        filepack: {
            read: true,
            write: false
        }
    }
}

describe('Admin permission guard', () => {
    it('Should allow firmware.read', async () => {
        const result = permissionGuardCheck('firmware.read', [ADMIN_ROLE, READONLY_ROLE]);
        expect(result).to.be.true;
    });

    it('Should allow firmware.write', async () => {
        const result = permissionGuardCheck('firmware.write', [ADMIN_ROLE, READONLY_ROLE]);
        expect(result).to.be.true;
    });
});

describe('Readonly permission guard', () => {

    it('Should allow filepack.read', async () => {
        const result = permissionGuardCheck('filepack.read', [READONLY_ROLE]);
        expect(result).to.be.true;
    });

    it('Should not allow filepack.write', async () => {
        const result = permissionGuardCheck('filepack.write', [READONLY_ROLE]);
        expect(result).to.be.false;
    });
});

describe('No role permission guard', () => {

    it('Should not allow filepack.read', async () => {
        const result = permissionGuardCheck('filepack.read', []);
        expect(result).to.be.false;
    });

    it('Should not allow filepack.write', async () => {
        const result = permissionGuardCheck('filepack.write', []);
        expect(result).to.be.false;
    });
});
