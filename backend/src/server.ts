import Express from 'express';
import bodyParser from 'body-parser';
import expressJSDocSwagger from 'express-jsdoc-swagger';
import cors from 'cors';

import jsdocExpressOptions from './jsdoc-swagger-conf';

import Logger from './logger';
import FirmwareRouter from './routes/firmware.route';
import FilepackRouter from './routes/filepack.route';
import AuthRouter from './routes/auth.route';
import { LogMiddleware } from './middlewares/log';

const APP_PORT: number = parseInt(process.env.APP_PORT ?? '');

const server = Express();
server.use(cors())
server.use(Express.json());
server.use(bodyParser.raw({ type: 'application/zip', limit: '10mb' }));
server.use(LogMiddleware)

expressJSDocSwagger(server)(jsdocExpressOptions);

server.use('/api', AuthRouter);
server.use('/api', FirmwareRouter);
server.use('/api', FilepackRouter);


server.listen(APP_PORT, () => {
    Logger.info(`Server started at :${APP_PORT}`);
});