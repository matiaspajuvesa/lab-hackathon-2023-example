import fs from 'fs/promises';
import path from 'path';

import { v4 as uuid } from 'uuid';
import Logger from '../logger';

import jwt from 'jsonwebtoken';
import { Role, User } from '../models/user';
import { UserLoginRequest, UserRegisterRequest } from '../request-response/auth.request';
import { UserLoginResponse, UserRegisterResponse } from '../request-response/auth.response';
import { mkdirp } from 'mkdirp'

// Hard code the admin role here
const ADMIN_ROLE: Role = {
    id: '4ba42d62-9462-449f-a90e-11e993ff48bf',
    name: 'admin-role',
    permissions: {
        firmware: {
            read: true,
            write: true
        },
        filepack: {
            read: true,
            write: true
        }
    }
}

export interface TokenPayload {
    username: string;
    roles: Role[];
}

export class UserController {

    private DATA_PATH = process.env.DATA_PATH ?? process.cwd();
    private FILE_ID = 'users.json';

    private async createIfNotExists() {
        const folder = path.resolve(this.DATA_PATH, 'database');
        const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
        try {
            await fs.stat(p);
        } catch (error) {
            await mkdirp(folder);
            await fs.writeFile(p, Buffer.from(JSON.stringify([])));
        }
    }

    async login(opts: UserLoginRequest): Promise<UserLoginResponse> {
        try {
            await this.createIfNotExists();

            if (opts.username == null || opts.password == null) {
                throw new Error('Bad request');
            }

            const p = path.resolve(this.DATA_PATH, 'database', 'users.json')
            const buf = await fs.readFile(p);
            const users: User[] = JSON.parse(buf.toString());
            const foundUser = users.find(f => f.username === opts.username);
            if (foundUser == null) {
                throw new Error(`User not found or invalid credentials`);
            }

            const payload: TokenPayload = {
                username: foundUser.username,
                roles: foundUser.roles
            }

            const JWT_SECRET = process.env.JWT_SECRET;
            if (JWT_SECRET == null) {
                throw new Error(`JWT_SECRET is missing`);
            }

            const signedToken = jwt.sign(payload, JWT_SECRET, {
                expiresIn: '24h',
                issuer: 'cloud-api',
                jwtid: uuid()
            });
            const decoded = jwt.decode(signedToken) as jwt.JwtPayload

            return {
                token: signedToken,
                validUntil: (decoded.exp ?? 0) * 1000
            } as UserLoginResponse;
        } catch (error) {
            Logger.error(`Failed to login`, error);
            throw new Error(`Failed to login`);
        }
    }

    // Use some proven authentication provider
    // E.g. Auth0: https://auth0.com/
    // Rolling with your own could be a great risk
    // But we love risks in hackathons so lets go with this
    async register(opts: UserRegisterRequest): Promise<UserRegisterResponse> {
        try {
            await this.createIfNotExists();

            if (opts.username == null || opts.password == null) {
                throw new Error('Bad request');
            }

            const newUser: User = {
                username: opts.username,
                password: opts.password,
                roles: [ADMIN_ROLE]
            };

            const p = path.resolve(this.DATA_PATH, 'database', 'users.json')
            const buf = await fs.readFile(p);
            const users: User[] = JSON.parse(buf.toString());
            users.push(newUser);

            const newBuf = Buffer.from(JSON.stringify(users, null, 2))
            await fs.writeFile(p, newBuf);

            return {
                success: true
            };
        } catch (error) {
            Logger.error(`Failed to create user`, error);
            return {
                success: false
            };
        }
    }
}