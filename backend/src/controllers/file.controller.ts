import fs from 'fs/promises';
import path from 'path';
import { mkdirp } from 'mkdirp'

import { v4 as uuid } from 'uuid';
import { FileAttachment } from '../models/file';
import Logger from '../logger';

import mime from 'mime-types';

export interface FileCreateOpts {
    name?: string;
    contentType: string;
}

export class FileAttachmentController {
    private DATA_PATH = process.env.DATA_PATH ?? process.cwd();
    private FILE_ID = 'files.json';

    private async createIfNotExists() {
        const folder = path.resolve(this.DATA_PATH, 'database');
        const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
        try {
            await fs.stat(p);
        } catch (error) {
            await mkdirp(folder);
            await fs.writeFile(p, Buffer.from(JSON.stringify([])));
        }
    }

    async create(file: Buffer, opts: FileCreateOpts): Promise<FileAttachment> {
        await this.createIfNotExists();

        const extensionFromContentType = mime.extension(opts.contentType);

        const id = uuid();
        const filename = `${id}.${extensionFromContentType}`;


        const folder = path.resolve(this.DATA_PATH, 'fs');
        const p = path.resolve(folder, filename);


        await mkdirp(folder);

        await fs.writeFile(p, file);

        const fileAttachment: FileAttachment = {
            id: id,
            name: opts.name,
            bytes: file.length,
            path: p,
            type: opts.contentType
        };

        const fileP = path.resolve(this.DATA_PATH, 'database', this.FILE_ID);
        let files: FileAttachment[] = [];

        try {
            await fs.stat(fileP);
            const buf = await fs.readFile(fileP);
            files = JSON.parse(buf.toString());
        } catch (error) {
            Logger.error(error);
        }
        files.push(fileAttachment);

        const newBuf = Buffer.from(JSON.stringify(files, null, 2))
        await fs.writeFile(fileP, newBuf);
        return fileAttachment;
    }

    async meta(id: string): Promise<FileAttachment | undefined> {
        await this.createIfNotExists();
        const fileP = path.resolve(this.DATA_PATH, 'database', this.FILE_ID);

        try {
            await fs.stat(fileP);
            const buf = await fs.readFile(fileP);
            const files: FileAttachment[] = JSON.parse(buf.toString());

            const found = files.find(f => f.id === id);
            if (found) {
                return found;
            }
            return undefined;
        } catch (error) {
            return undefined;
        }
    }

    async get(id: string): Promise<Buffer> {
        const meta = await this.meta(id);
        if (meta == null) {
            throw new Error('File does not exist');
        }

        const buf = await fs.readFile(meta.path);
        return buf;
    }

    async delete(id: string): Promise<boolean> {
        throw new Error();
    }
}