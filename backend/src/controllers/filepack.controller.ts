import fs from 'fs/promises';
import path from 'path';

import { v4 as uuid } from 'uuid';
import Logger from '../logger';
import { Filepack } from '../models/filepack';
import { FilepackCreateRequest } from '../request-response/filepack.request';
import { mkdirp } from 'mkdirp'


export class FilepackController {

    private DATA_PATH = process.env.DATA_PATH ?? process.cwd();
    private FILE_ID = 'filepack.json';

    private async createIfNotExists() {
        const folder = path.resolve(this.DATA_PATH, 'database');
        const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
        try {
            await fs.stat(p);
        } catch (error) {
            await mkdirp(folder);
            await fs.writeFile(p, Buffer.from(JSON.stringify([])));
        }
    }

    async list(): Promise<Filepack[]> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const filepacks: Filepack[] = JSON.parse(buf.toString());
            return filepacks;
        } catch (error) {
            Logger.error(`Failed to list filepack`, error);
            return [];
        }
    }

    async get(id: string): Promise<Filepack | undefined> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const filepacks: Filepack[] = JSON.parse(buf.toString());
            return filepacks.find(f => f.id === id);
        } catch (error) {
            Logger.error(`Failed to get filepack`, error);
            return undefined;
        }
    }

    async create(body: FilepackCreateRequest): Promise<Filepack | undefined> {
        try {
            await this.createIfNotExists();

            if (body == null || body.deviceId == null || body.name == null || body.version == null) {
                throw new Error('Bad request');
            }

            const newFilepack: Filepack = {
                id: uuid(),
                deviceId: body.deviceId,
                name: body.name,
                version: body.version,
                description: body.description,
                type: 'filepack'
            };

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)

            let filepacks = [];
            try {
                await fs.stat(p);
                const buf = await fs.readFile(p);
                filepacks = JSON.parse(buf.toString());
            } catch (error) {
                Logger.error(`Failed to read filepacks`, error);
            }

            filepacks.push(newFilepack);

            const newBuf = Buffer.from(JSON.stringify(filepacks, null, 2))
            await fs.writeFile(p, newBuf);

            return newFilepack;
        } catch (error) {
            Logger.error(`Failed to create filepack`, error);
            return undefined;
        }
    }

    async delete(id: string): Promise<boolean> {
        try {
            await this.createIfNotExists();

            const filepacks = await this.list();
            const found = filepacks.find(f => f.id === id);

            if (found) {
                const idx = filepacks.findIndex(f => f.id === id);
                if (idx >= 0) {
                    filepacks.splice(idx, 1);
                }

                const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
                const newBuf = Buffer.from(JSON.stringify(filepacks, null, 2))
                await fs.writeFile(p, newBuf);
                return true;
            }

            return false;
        } catch (error) {
            Logger.error(`Failed to delete filepack`, error);
            return false;
        }
    }

    async patch(id: string, body: Partial<Filepack>): Promise<boolean> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const filepacks: Filepack[] = JSON.parse(buf.toString());
            const found = filepacks.find(f => f.id === id);

            if (found) {
                const idx = filepacks.findIndex(f => f.id === id);
                if (idx >= 0) {
                    filepacks.splice(idx, 1);
                }

                const newFilepack: Filepack = {
                    ...found,
                    ...body
                };
                filepacks.push(newFilepack);

                const newBuf = Buffer.from(JSON.stringify(filepacks, null, 2))
                await fs.writeFile(p, newBuf);
                return true;
            }

            return false;
        } catch (error) {
            Logger.error(`Failed to patch filepack`, error);
            return false;
        }
    }
}