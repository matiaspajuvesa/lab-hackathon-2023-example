import fs from 'fs/promises';
import path from 'path';
import { Firmware } from '../models/firmware';

import { v4 as uuid } from 'uuid';
import { FirmwareCreateRequest } from '../request-response/firmware.request';
import Logger from '../logger';
import { mkdirp } from 'mkdirp'


export class FirmwareController {

    private DATA_PATH = process.env.DATA_PATH ?? process.cwd();
    private FILE_ID = 'firmwares.json';

    private async createIfNotExists() {
        const folder = path.resolve(this.DATA_PATH, 'database');
        const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
        try {
            await fs.stat(p);
        } catch (error) {
            await mkdirp(folder);
            await fs.writeFile(p, Buffer.from(JSON.stringify([])));
        }
    }

    async list(): Promise<Firmware[]> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const firmwares: Firmware[] = JSON.parse(buf.toString());
            return firmwares;
        } catch (error) {
            Logger.error(`Failed to list firmwares`, error);
            return [];
        }
    }

    async get(id: string): Promise<Firmware | undefined> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const firmwares: Firmware[] = JSON.parse(buf.toString());
            return firmwares.find(f => f.id === id);
        } catch (error) {
            Logger.error(`Failed to get firmware`, error);
            return undefined;
        }
    }

    async create(body: FirmwareCreateRequest): Promise<Firmware | undefined> {
        try {
            await this.createIfNotExists();

            if (body == null || body.deviceId == null || body.name == null || body.version == null) {
                throw new Error('Bad request');
            }

            const newFirmware: Firmware = {
                id: uuid(),
                deviceId: body.deviceId,
                name: body.name,
                version: body.version,
                description: body.description,
                type: 'firmware'
            };

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const firmwares: Firmware[] = JSON.parse(buf.toString());
            firmwares.push(newFirmware);

            const newBuf = Buffer.from(JSON.stringify(firmwares, null, 2))
            await fs.writeFile(p, newBuf);

            return newFirmware;
        } catch (error) {
            Logger.error(`Failed to create firmware`, error);
            return undefined;
        }
    }

    async delete(id: string): Promise<boolean> {
        try {
            await this.createIfNotExists();

            const firmwares = await this.list();
            const found = firmwares.find(f => f.id === id);

            if (found) {
                const idx = firmwares.findIndex(f => f.id === id);
                if (idx >= 0) {
                    firmwares.splice(idx, 1);
                }

                const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
                const newBuf = Buffer.from(JSON.stringify(firmwares, null, 2))
                await fs.writeFile(p, newBuf);
                return true;
            }

            return false;
        } catch (error) {
            Logger.error(`Failed to delete firmware`, error);
            return false;
        }
    }

    async patch(id: string, body: Partial<Firmware>): Promise<boolean> {
        try {
            await this.createIfNotExists();

            const p = path.resolve(this.DATA_PATH, 'database', this.FILE_ID)
            const buf = await fs.readFile(p);
            const firmwares: Firmware[] = JSON.parse(buf.toString());
            const found = firmwares.find(f => f.id === id);

            if (found) {
                const idx = firmwares.findIndex(f => f.id === id);
                if (idx >= 0) {
                    firmwares.splice(idx, 1);
                }

                const newFirmware: Firmware = {
                    ...found,
                    ...body
                };
                firmwares.push(newFirmware);

                const newBuf = Buffer.from(JSON.stringify(firmwares, null, 2))
                await fs.writeFile(p, newBuf);
                return true;
            }

            return false;
        } catch (error) {
            Logger.error(`Failed to patch firmware`, error);
            return false;
        }
    }
}