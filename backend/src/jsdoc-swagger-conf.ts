export default {
  info: {
    version: '1.0.0',
    title: 'Kemppi Software HUB',
    license: {
      name: 'MIT',
    },
  },
  swaggerUIPath: '/docs/api',
  exposeSwaggerUI: true,
  exposeApiDocs: true,
  apiDocsPath: '/docs/v3/api',
  baseDir: __dirname,
  // Glob pattern to find your jsdoc files (multiple patterns can be added in an array)
  filesPattern: ['./routes/*.route.js', './models/*.d.ts', './request-response/*.d.ts'],
};