import { NextFunction, Request, Response } from "express";
import Logger from '../logger';

export function LogMiddleware(req: Request, res: Response, next: NextFunction) {
    if (Logger.level() >= 30) {
        Logger.info(`${req.method} ${req.path}`);
    }
    Logger.debug(`${req.method} ${req.path}`);
    next();
}