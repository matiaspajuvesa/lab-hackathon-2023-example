import { NextFunction, Request, Response } from "express";
import Logger from '../logger';
import dot from 'dot-object';

import { TokenPayload } from "../controllers/user.controller";
import { Role } from "../models/user";

export type AuthenticatedRequest = Request & { authenticated: TokenPayload }


export function PermissionGuardMiddleware(permission: string) {
    return (req: Request, res: Response, next: NextFunction) => {
        const user = (req as AuthenticatedRequest).authenticated;

        if (permissionGuardCheck(permission, user.roles)) {
            return next();
        }

        Logger.info(`No permission for this user ${user.username} for action ${permission}`)
        return res.status(401).end();
    }
}

export function permissionGuardCheck(permission: string, roles: Role[]): boolean {
    for (const role of roles) {
        const dotFormat = dot.dot(role.permissions);
        if (dotFormat[permission] === true) {
            return true;
        }
    }
    return false;
}
