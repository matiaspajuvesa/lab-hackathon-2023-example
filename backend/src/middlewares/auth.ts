import { NextFunction, Request, Response } from "express";
import Logger from '../logger';

import jwt from 'jsonwebtoken';
import { TokenPayload } from "../controllers/user.controller";

export type AuthenticatedRequest = Request & { authenticated: TokenPayload }


export function AuthGuardMiddleware(req: Request, res: Response, next: NextFunction) {
    Logger.debug(`${req.method} ${req.path} - Authenticated route called checking credentials`);
    const authHeader = req.headers.authorization;
    if (authHeader == null) {
        Logger.info(`Authorization header not provided`)
        return res.status(401).end();
    }

    const token = authHeader.replace(/^Bearer +/, '');

    if (token == null || token.length <= 0) {
        return res.status(401).end();
    }

    const JWT_SECRET = process.env.JWT_SECRET;
    if (JWT_SECRET == null) {
        Logger.error(`JWT_SECRET is nil in auth guard`);
        return res.status(401).end();
    }


    try {
        const verified = jwt.verify(token, JWT_SECRET);

        // @ts-ignore
        req['authenticated'] = verified;
        next();
    } catch (error) {
        Logger.error(`Failed to authenticate`, error);
        return res.status(401).end();
    }

    if (authHeader === 'admin') {
        Logger.debug(`Authenticated as admin, continuing`)
        next();
    }
}