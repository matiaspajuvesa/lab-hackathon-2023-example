/**
 * @typedef {object} FirmwareDeleteResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FirmwareDeleteResponse {
    id: string;
    message: string;
}

/**
 * @typedef {object} FirmwarePatchResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FirmwarePatchResponse {
    id: string;
    message: string;
}

/**
 * @typedef {object} FirmwareFileAttachResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FirmwareFileAttachResponse {
    id: string;
    message: string;
}
