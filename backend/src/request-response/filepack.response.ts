/**
 * @typedef {object} FilepackDeleteResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FilepackDeleteResponse {
    id: string;
    message: string;
}

/**
 * @typedef {object} FilepackPatchResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FilepackPatchResponse {
    id: string;
    message: string;
}

/**
 * @typedef {object} FilepackFileAttachResponse
 * @property {string} id.required - File id
 * @property {string} message.required - Message about what happened
 */
export interface FilepackFileAttachResponse {
    id: string;
    message: string;
}
