/**
 * @typedef {object} UserLoginResponse
 * @property {string} token.required - Token
 * @property {string} validUntil.required - When the token expires
 */
export interface UserLoginResponse {
    token: string;
    validUntil: number;
}

/**
 * @typedef {object} UserRegisterResponse
 * @property {boolean} success.required - Success state
 */
export interface UserRegisterResponse {
    success: boolean;
}
