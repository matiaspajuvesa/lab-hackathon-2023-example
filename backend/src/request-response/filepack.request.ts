import { Filepack } from "../models/filepack";

/**
 * @typedef {object} FilepackCreateRequest
 * @property {string} deviceId.required - Device typ
 * @property {string} name.required - Name for the firmware
 */
export type FilepackCreateRequest = Partial<Filepack>;
