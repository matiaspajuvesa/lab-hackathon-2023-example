import { Firmware } from "../models/firmware";

/**
 * @typedef {object} FirmwareCreateRequest
 * @property {string} deviceId.required - Device typ
 * @property {string} name.required - Name for the firmware
 */
export type FirmwareCreateRequest = Partial<Firmware>;
