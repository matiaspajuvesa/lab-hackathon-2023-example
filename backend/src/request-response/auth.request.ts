/**
 * @typedef {object} UserRegisterRequest
 * @property {string} username.required - Username
 * @property {string} password.required - Password
 */
export interface UserRegisterRequest {
    username: string;
    password: string;
}


/**
 * @typedef {object} UserLoginRequest
 * @property {string} username.required - Username
 * @property {string} password.required - Password
 */
export interface UserLoginRequest {
    username: string;
    password: string;
}