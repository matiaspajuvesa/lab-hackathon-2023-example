import Bunyan from 'bunyan';
const LOG_LEVEL: Bunyan.LogLevel = process.env.LOG_LEVEL as Bunyan.LogLevel ?? Bunyan.INFO;

export default Bunyan.createLogger({ name: 'app_logger', level: LOG_LEVEL });