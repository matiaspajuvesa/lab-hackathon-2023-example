import { Router } from 'express';
import { FileAttachmentController, FileCreateOpts } from '../controllers/file.controller';

import Logger from '../logger';
import { AuthGuardMiddleware } from '../middlewares/auth';
import { PermissionGuardMiddleware } from '../middlewares/permission';
import { Filepack } from '../models/filepack';
import { FilepackController } from '../controllers/filepack.controller';
import { FilepackCreateRequest } from '../request-response/filepack.request';
import { FilepackDeleteResponse, FilepackFileAttachResponse, FilepackPatchResponse } from '../request-response/filepack.response';

const router = Router();

/**
 * GET /api/filepack
 * @summary Returns a list of filepack files
 * @tags Filepack
 * @return {Filepack[]} 200 - success reponse
 * @example response - 200 - Example success response
    [
        {
            "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
            "deviceId": "X5",
            "name": "1.66.00",
            "type": "filepack",
            "file": {
                "id": "9f5b707a-25af-4c57-bea0-304dd0ae70d2",
                "name": "update_16600.zip",
                "bytes": 6064407,
                "path": "/fs/9f5b707a-25af-4c57-bea0-304dd0ae70d2.zip",
                "type": "application/zip"
            }
        },
        {
            "id": "84cd01d3-4803-456b-8b08-f8bdfcf1a028",
            "type": "filepack",
            "name": "1.00.00",
            "deviceId": "X5"
        }
    ]
 */
router.get('/filepack', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);
    const fc = new FilepackController();
    const filepack = await fc.list();
    res.json(filepack);
});

/**
 * POST /api/filepack
 * @summary Creates a new filepack
 * @tags Filepack
 * @param {FilepackCreateRequest} request.body.required - Filepack body to create
 * @return {Filepack} 200 - success response
 * @example request - Example request
    {
        "type": "filepack",
        "name": "1.20.00",
        "deviceId": "X5"
    }
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "deviceId": "X5",
        "name": "1.20.00",
        "type": "filepack"
    }
 */
router.post('/filepack', AuthGuardMiddleware, PermissionGuardMiddleware('filepack.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    const fc = new FilepackController();
    const filepack = await fc.create(req.body as FilepackCreateRequest);

    res.json(filepack);
});

/**
 * GET /api/filepack/{id}
 * @summary Returns a single filepack
 * @tags Filepack
 * @param {string} id.path - Filepack id
 * @return {Filepack} 200 - success response
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "deviceId": "X5",
        "name": "1.66.00",
        "type": "filepack",
        "file": {
            "id": "9f5b707a-25af-4c57-bea0-304dd0ae70d2",
            "name": "update_16600.zip",
            "bytes": 6064407,
            "path": "/fs/9f5b707a-25af-4c57-bea0-304dd0ae70d2.zip",
            "type": "application/zip"
        }
    }
 */
router.get('/filepack/:id', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    const id: string = req.params.id;

    const fc = new FilepackController();
    const filepack = await fc.get(id);

    res.json(filepack);
});

/**
 * PATCH /api/filepack/{id}
 * @summary Updates a single filepack
 * @tags Filepack
 * @param {string} id.path - Filepack id
 * @param {Filepack} request.body.required - Filepack body to update
 * @return {FilepackPatchResponse} 200 - success response
 * @example request - Example request
    {
        "name":"1.20.00"
    }
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "message": "Filepack 1ac0ba44-82aa-4c07-a017-b8eaa28223f9 updated"
    }
 */
router.patch('/filepack/:id', AuthGuardMiddleware, PermissionGuardMiddleware('filepack.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    // const user = (req as AuthenticatedRequest).authenticated
    // console.log(user);

    const id: string = req.params.id;
    const body = req.body;

    const fc = new FilepackController();
    const filepack = await fc.patch(id, body);

    res.json({
        id: id,
        message: `Filepack ${id} updated`
    } as FilepackPatchResponse);
});

/**
 * DELETE /api/filepack/{id}
 * @summary Deletes a single filepack
 * @tags Filepack
 * @param {string} id.path - Filepack id
 * @return {FilepackDeleteResponse} 200 - Success response
 * @example response - 200 - Example success response
    {
        "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
        "message": "Filepack 0aed0801-c437-40ce-aed4-c6f8eb8ebb43 deleted"
    }
 * @return {FilepackDeleteResponse} 404 - Failure response
 * @example response - 404 - Example failure response
    {
    "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
    "message": "Failed to delete 0aed0801-c437-40ce-aed4-c6f8eb8ebb43"
    }
 */
router.delete('/filepack/:id', AuthGuardMiddleware, PermissionGuardMiddleware('filepack.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    const fc = new FilepackController();
    const deleted = await fc.delete(req.params.id);

    if (deleted) {
        res.json({
            id: req.params.id,
            message: `Filepack ${req.params.id} deleted`
        } as FilepackDeleteResponse);
    } else {
        res.status(404).json({
            id: req.params.id,
            message: `Failed to delete ${req.params.id}`
        } as FilepackDeleteResponse);
    }
});

/**
 * POST /api/filepack/{id}/upload
 * @summary Uploads a filepack attachment
 * @tags Filepack
 * @param {string} id.path - Filepack id
 * @return {FilepackFileAttachResponse} 200 - Success response
 * @example response - 200 - Example success response
    {
        "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
        "message": "Attached 0aed0801-c437-40ce-aed4-c6f8eb8ebb43 to 1ac0ba44-82aa-4c07-a017-b8eaa28223f9"
    }
 */
router.post('/filepack/:id/upload', AuthGuardMiddleware, PermissionGuardMiddleware('filepack.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    const id = req.params.id;

    const fac = new FileAttachmentController();
    const opts: FileCreateOpts = {
        name: req.query.name?.toString() ?? undefined,
        contentType: req.headers['content-type'] ?? ''
    }
    const createdFile = await fac.create(req.body, opts);

    const fc = new FilepackController();
    const attachedFiled = await fc.patch(id, { file: createdFile });

    res.json({
        id: createdFile.id,
        message: `Attached ${createdFile.id} file to ${id}`
    } as FilepackFileAttachResponse);
});

/**
 * GET /api/filepack/{id}/download
 * @summary Downloads a filepack attachment
 * @tags Filepack
 * @param {string} id.path - Filepack id
 * @return {string} 200 - Success response
 */
router.get('/filepack/:id/download', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} filepack.router called`);

    const id = req.params.id;

    const fc = new FilepackController();
    const fm = await fc.get(id);

    if (fm?.file?.id == null) {
        return res.status(404).json({});
    }

    const fac = new FileAttachmentController();
    const meta = await fac.meta(fm.file.id);
    const file = await fac.get(fm.file.id);
    if (meta == null || file == null) {
        return res.status(404).json({});
    }

    if (meta.name) {
        res.setHeader('Content-disposition', `attachment; filename=${meta.name}`);
    }

    res.setHeader('Content-type', meta.type);
    res.send(file);
});

export default router;