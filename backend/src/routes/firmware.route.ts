import { Router } from 'express';
import { FirmwareController } from '../controllers/firmware.controller';
import { FileAttachmentController, FileCreateOpts } from '../controllers/file.controller';

import Logger from '../logger';
import { FirmwareDeleteResponse, FirmwareFileAttachResponse, FirmwarePatchResponse } from '../request-response/firmware.response';
import { FirmwareCreateRequest } from '../request-response/firmware.request';
import { AuthGuardMiddleware } from '../middlewares/auth';
import { PermissionGuardMiddleware } from '../middlewares/permission';

const router = Router();

/**
 * GET /api/firmware
 * @summary Returns a list of firmware files
 * @tags Firmware
 * @return {Firmware[]} 200 - success reponse
 * @example response - 200 - Example success response
    [
        {
            "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
            "deviceId": "X5",
            "name": "1.66.00",
            "type": "firmware",
            "file": {
                "id": "9f5b707a-25af-4c57-bea0-304dd0ae70d2",
                "name": "update_16600.zip",
                "bytes": 6064407,
                "path": "/fs/9f5b707a-25af-4c57-bea0-304dd0ae70d2.zip",
                "type": "application/zip"
            }
        },
        {
            "id": "84cd01d3-4803-456b-8b08-f8bdfcf1a028",
            "type": "firmware",
            "name": "1.00.00",
            "deviceId": "X5"
        }
    ]
 */
router.get('/firmware', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);
    const fc = new FirmwareController();
    const firmware = await fc.list();
    res.json(firmware);
});

/**
 * POST /api/firmware
 * @summary Creates a new firmware
 * @tags Firmware
 * @param {FirmwareCreateRequest} request.body.required - Firmware body to create
 * @return {Firmware} 200 - success response
 * @example request - Example request
    {
        "type": "firmware",
        "name": "1.20.00",
        "deviceId": "X5"
    }
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "deviceId": "X5",
        "name": "1.20.00",
        "type": "firmware"
    }
 */
router.post('/firmware', AuthGuardMiddleware, PermissionGuardMiddleware('firmware.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    const fc = new FirmwareController();
    const firmware = await fc.create(req.body as FirmwareCreateRequest);

    res.json(firmware);
});

/**
 * GET /api/firmware/{id}
 * @summary Returns a single firmware
 * @tags Firmware
 * @param {string} id.path - Firmware id
 * @return {Firmware} 200 - success response
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "deviceId": "X5",
        "name": "1.66.00",
        "type": "firmware",
        "file": {
            "id": "9f5b707a-25af-4c57-bea0-304dd0ae70d2",
            "name": "update_16600.zip",
            "bytes": 6064407,
            "path": "/fs/9f5b707a-25af-4c57-bea0-304dd0ae70d2.zip",
            "type": "application/zip"
        }
    }
 */
router.get('/firmware/:id', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    const id: string = req.params.id;

    const fc = new FirmwareController();
    const firmware = await fc.get(id);

    res.json(firmware);
});

/**
 * PATCH /api/firmware/{id}
 * @summary Updates a single firmware
 * @tags Firmware
 * @param {string} id.path - Firmware id
 * @param {Firmware} request.body.required - Firmware body to update
 * @return {FirmwarePatchResponse} 200 - success response
 * @example request - Example request
    {
        "name":"1.20.00"
    }
 * @example response - 200 - Example success response
    {
        "id": "1ac0ba44-82aa-4c07-a017-b8eaa28223f9",
        "message": "Firmware 1ac0ba44-82aa-4c07-a017-b8eaa28223f9 updated"
    }
 */
router.patch('/firmware/:id', AuthGuardMiddleware, PermissionGuardMiddleware('firmware.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    // const user = (req as AuthenticatedRequest).authenticated
    // console.log(user);

    const id: string = req.params.id;
    const body = req.body;

    const fc = new FirmwareController();
    const firmware = await fc.patch(id, body);

    res.json({
        id: id,
        message: `Firmware ${id} updated`
    } as FirmwarePatchResponse);
});

/**
 * DELETE /api/firmware/{id}
 * @summary Deletes a single firmware
 * @tags Firmware
 * @param {string} id.path - Firmware id
 * @return {FirmwareDeleteResponse} 200 - Success response
 * @example response - 200 - Example success response
    {
        "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
        "message": "Firmware 0aed0801-c437-40ce-aed4-c6f8eb8ebb43 deleted"
    }
 * @return {FirmwareDeleteResponse} 404 - Failure response
 * @example response - 404 - Example failure response
    {
    "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
    "message": "Failed to delete 0aed0801-c437-40ce-aed4-c6f8eb8ebb43"
    }
 */
router.delete('/firmware/:id', AuthGuardMiddleware, PermissionGuardMiddleware('firmware.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    const fc = new FirmwareController();
    const deleted = await fc.delete(req.params.id);

    if (deleted) {
        res.json({
            id: req.params.id,
            message: `Firmware ${req.params.id} deleted`
        } as FirmwareDeleteResponse);
    } else {
        res.status(404).json({
            id: req.params.id,
            message: `Failed to delete ${req.params.id}`
        } as FirmwareDeleteResponse);
    }
});

/**
 * POST /api/firmware/{id}/upload
 * @summary Uploads a firmware attachment
 * @tags Firmware
 * @param {string} id.path - Firmware id
 * @return {FirmwareFileAttachResponse} 200 - Success response
 * @example response - 200 - Example success response
    {
        "id": "0aed0801-c437-40ce-aed4-c6f8eb8ebb43",
        "message": "Attached 0aed0801-c437-40ce-aed4-c6f8eb8ebb43 to 1ac0ba44-82aa-4c07-a017-b8eaa28223f9"
    }
 */
router.post('/firmware/:id/upload', AuthGuardMiddleware, PermissionGuardMiddleware('firmware.write'), async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    const id = req.params.id;

    const fac = new FileAttachmentController();
    const opts: FileCreateOpts = {
        name: req.query.name?.toString() ?? undefined,
        contentType: req.headers['content-type'] ?? ''
    }
    const createdFile = await fac.create(req.body, opts);

    const fc = new FirmwareController();
    const attachedFiled = await fc.patch(id, { file: createdFile });

    res.json({
        id: createdFile.id,
        message: `Attached ${createdFile.id} file to ${id}`
    } as FirmwareFileAttachResponse);
});

/**
 * GET /api/firmware/{id}/download
 * @summary Downloads a firmware attachment
 * @tags Firmware
 * @param {string} id.path - Firmware id
 * @return {string} 200 - Success response
 */
router.get('/firmware/:id/download', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} firmware.router called`);

    const id = req.params.id;

    const fc = new FirmwareController();
    const fm = await fc.get(id);

    if (fm?.file?.id == null) {
        return res.status(404).json({});
    }

    const fac = new FileAttachmentController();
    const meta = await fac.meta(fm.file.id);
    const file = await fac.get(fm.file.id);
    if (meta == null || file == null) {
        return res.status(404).json({});
    }

    if (meta.name) {
        res.setHeader('Content-disposition', `attachment; filename=${meta.name}`);
    }

    res.setHeader('Content-type', meta.type);
    res.send(file);
});

export default router;