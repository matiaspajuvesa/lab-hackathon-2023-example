import { Router } from 'express';
import { UserController } from '../controllers/user.controller';
import Logger from '../logger';

const router = Router();

/**
 * POST /api/auth/login
 * @summary User login
 * @tags Auth
 * @param {UserLoginRequest} request.body.required - Firmware body to create
 * @return {UserLoginResponse} 200 - success response
 * @example request - Example request
    {
        "username": "matias",
        "password": "testi"
    }
 * @example response - 200 - Example success response
    {
        "token": "eyJ...",
        "validUntil": 1676094805000
    }
 */
router.post('/auth/login', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} auth.router called`);

    try {
        const uc = new UserController();
        const loggedIn = await uc.login({ username: req.body.username, password: req.body.password });
        res.json(loggedIn);
    } catch (error) {
        res.status(400).json({ message: `Failed to login` });
    }
});

/**
 * POST /api/auth/register
 * @summary User register
 * @tags Auth
 * @param {UserRegisterRequest} request.body.required - Firmware body to create
 * @return {UserRegisterResponse} 200 - success response
 * @example request - Example request
    {
        "username": "matias",
        "password": "testi"
    }
 * @example response - 200 - Example success response
    {
        "success": true
    }
 */
router.post('/auth/register', async (req, res, next) => {
    Logger.debug(`${req.method} ${req.path} auth.router called`);

    try {
        const uc = new UserController();
        const registered = await uc.register({ username: req.body.username, password: req.body.password });
        res.json(registered);
    } catch (error) {
        res.status(400).json({ message: `Failed to register` });
    }

});

export default router;