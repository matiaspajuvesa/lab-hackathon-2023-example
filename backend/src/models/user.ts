export interface User {
    username: string;
    password: string;
    roles: Role[];
}


export interface Role {
    id: string;
    name: string;
    permissions: {
        firmware?: Permission;
        filepack?: Permission;
    }
}

export interface Permission {
    read: boolean;
    write: boolean;
}