
/**
 * File attachment
 * @typedef {object} FileAttachment
 * @property {string} id.required - Id for the file
 * @property {string} name - Name for the file
 * @property {string} type.required - File type
 * @property {number} bytes.required - Byte size
 * @property {string} path.required - Path in disk
 */
export interface FileAttachment {
    id: string;
    name?: string;
    type: string;
    bytes: number;
    path: string;
}

export interface File {
    id: string;
    name: string;
    version: string;
    deviceId: string;
    description?: string;
    type: 'filepack' | 'firmware';
    file?: FileAttachment;
}

