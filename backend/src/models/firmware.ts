import { File } from './file';

/**
 * Firmware
 * @typedef {object} Firmware
 * @property {string} id.required - Id of the firmware
 * @property {string} name.required - Name of the firmware
 * @property {string} version.required - Version of the firmware
 * @property {string} deviceId.required - Device id for which this firmware is
 * @property {string} description.required - Description for this firmware
 * @property {string} type.required - Type of the package: 'firmware'
 * @property {FileAttachment} file.required - File attachment for this firmware
 */
export interface Firmware extends File {
    type: 'firmware';
}