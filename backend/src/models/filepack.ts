import { File, FileAttachment } from './file';

/**
 * Filepack
 * @typedef {object} Filepack
 * @property {string} id.required - Id of the package
 * @property {string} name.required - Name of the package
 * @property {string} version.required - Version of the firmware
 * @property {string} deviceId.required - Device id for which this firmware is
 * @property {string} description.required - Description for this firmware
 * @property {string} type.required - Type of the package: 'filepack'
 * @property {FileAttachment} file.required - File attachment for this filepack
 */
export interface Filepack extends File {
    type: 'filepack';
}